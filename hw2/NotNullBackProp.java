// This is an interesting case of back propagation of field information 
// since information about f must be passed back multiple timesin order 
// to get back to the girlpool() function and decide that f is not null at
// the time it calls foo.

class NotNullBackProp {
    public static void main(String[] a){
        System.out.println(new A().girlpool());
    }
}

class A {
    A f;

    public int girlpool(){
    	System.out.println(this.csh());
        return f.foo();
    }

    public int csh(){
    	System.out.println(this.weezer());
    	return 3;
    }

    public int weezer(){
    	f = new A();
    	return 2;
    }

    public int foo() {
    	return 1;
    }
}


