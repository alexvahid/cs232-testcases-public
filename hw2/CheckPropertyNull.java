//This testcase checks that you properly take a least upper bound
//on property constraints when going through message sends.

class Test {
    public static void main(String[] args) {
        A a;
        A b;
        a = new A();
        b = new A();
        System.out.println(b.setOtherA(a));
        System.out.println(a.f(b));
    }
}
class A {
    X x;
    A otherA;
    X nullX;

    public int f(A a) {
        int blah;

        x = new X();
        blah = a.g();
        return x.m();
    }
    public int g() {
        x = new X();
        System.out.println(this.setOtherX(nullX));
        return 0;
    }
    public int setOtherA(A a) {
        otherA = a;
        return 0;
    }
    public int setX(X newX) {
        x = newX;
        return 0;
    }
    public int setOtherX(X newX) {
        System.out.println(otherA.setX(newX));
        return 0;
    }
}
class X {
    public int m() {
        return 42;
    }
}